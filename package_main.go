package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func main() {
	var tok, _ = GetToken()
	log.Println(GetMyClassLessons(tok))
}

func GetToken() (Token string, ERROR error) {
	message := map[string]interface{}{
		"apiKey": "JSq9EzavVt2BOSiRfB9Nu5jB6dlqkPIdh5FfGQmtgHLkp2q3dw",
	}

	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		return "nil", err
	}

	resp, err := http.Post("https://api.moyklass.com/v1/company/auth/getToken", "application/json", bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		return "nil", err
	}

	var result map[string]string
	json.NewDecoder(resp.Body).Decode(&result)

	return result["accessToken"], nil
}
//ВНИМАНИЕ!!!!!!!
// map{string}interface{} нужен, что бы просто сразу увидеть, то какие данные мы получаем. Делать структуру пока НЕ надо!!! 
// Нужно сначала научиться получать данные, а потом за пару минут переделать в заполнение структур.
func GetMyClassLessons(Token string) (Result map[string]interface{}, Error error) {
	var answer map[string]interface{} // 
	jsonM := map[string]interface{}{
		"includeRecords": "true",
	}
	data, err := json.Marshal(jsonM)

	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://api.moyklass.com/v1/company/lessons", bytes.NewBuffer(data))
	if err != nil {
		return answer, err
	}
	req.Header.Set("x-access-token", Token)
	req.Header.Set("includeRecords", "true")

	if err != nil {
		return answer, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return answer, err
	}
	fmt.Println(resp)
	err = json.NewDecoder(resp.Body).Decode(&answer)
	if err != nil {
		return answer, err
	}
	return answer, err
}
