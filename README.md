package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
)

func main() {
	println(StudentInfo(GetToken(1)))
}

// -------------------------------------------------------------------------------------------------------------------------------------------
// Получение токена (он нужен для каждого запроса)

func GetToken(road int) (a string) {
	message := map[string]interface{}{
		"apiKey": "JSq9EzavVt2BOSiRfB9Nu5jB6dlqkPIdh5FfGQmtgHLkp2q3dw",
	}

	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		log.Fatalln("ошибочка", err)
	}

	resp, err := http.Post("https://api.moyklass.com/v1/company/auth/getToken", "application/json", bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		log.Fatalln(err)
	}
	var result map[string]string
	json.NewDecoder(resp.Body).Decode(&result)
	return (result["accessToken"])
}

// -------------------------------------------------------------------------------------------------------------------------------------------
// Удаление токена (это делается после каждого запроса)

// func DeleteToken(Token int) (a map[string]interface{}) {

// 	message := map[string]interface{}{
// 		"accessToken": Token,
// 	}

// 	bytesRepresentation, err := json.Marshal(message)
// 	if err != nil {
// 		log.Fatalln("ошибочка", err)
// 	}

// 	resp, err := http.Post("https://api.moyklass.com/v1/company/auth/revokeToken", "application/json", bytes.NewBuffer(bytesRepresentation))
// 	if err != nil {
// 		log.Fatalln(err)
// 	}
// 	var result map[string]interface{}

// 	json.NewDecoder(resp.Body).Decode(&result)
// 	return (result)
// }

// -------------------------------------------------------------------------------------------------------------------------------------------
// Просмотор инормации о конкретном студенте школы

// func StudentInfo(road int) (Info map[string]interface{}) {

// 	var Token = GetToken

// 	message := map[string]interface{}{
// 		"x-access-token": Token,
// 	}

// 	bytesRepresentation, err := json.Marshal(message)
// 	if err != nil {
// 		log.Fatalln("ошибочка", err)
// 	}

// 	resp := http.Get("https://api.moyklass.com/v1/company/users/1158767").http.Post("https://api.moyklass.com/v1/company/users/1158767", "application/json", bytes.NewBuffer(bytesRepresentation))
// 	if err != nil {
// 		log.Fatalln(err)
// 	}

// 	var result map[string]interface{}

// 	json.NewDecoder(resp.Body).Decode(&result)
// 	return (result)
// }

// -------------------------------------------------------------------------------------------------------------------------------------------
// Добавление в регистр сайта "Мой класс" нового студента

// func CreateStudent(road int) (confirmation map[string]interface{}) {
// 	message := map[string]interface{}{
// 		"apiKey": "JSq9EzavVt2BOSiRfB9Nu5jB6dlqkPIdh5FfGQmtgHLkp2q3dw",
// 	}

// 	bytesRepresentation, err := json.Marshal(message)
// 	if err != nil {
// 		log.Fatalln("ошибочка", err)
// 	}

// 	resp, err := http.Post("https://api.moyklass.com/v1/company/auth/getToken", "application/json", bytes.NewBuffer(bytesRepresentation))
// 	if err != nil {
// 		log.Fatalln(err)
// 	}
// 	var result map[string]interface{}
// 	json.NewDecoder(resp.Body).Decode(&result)
// 	return (result)
// }

func StudentInfo(Token string) (Info *http.Response) {

	resp, err := http.Get("https://api.moyklass.com/v1/company/users/{1158767}")

	if err != nil {
		log.Fatalln(err)
	}
	var result map[string]string
	println(result)
	json.NewDecoder(resp.Body).Decode(&result)

	return (resp)
}

// \"%s\ - вставить значение переменной в строку

